package main

import (
    "flag"
    "fmt"
    "io"
    "net/http"
    "net/url"
    "os"
    "sort"
    "strings"
    "sync"
    "time"
    "golang.org/x/net/html"
    "github.com/dustin/go-humanize"
)

// visit appends to links each link found in n and returns the result.
func visit(links []string, n *html.Node) []string {
    if (n == nil) {
        return nil
    }

    //fmt.Fprintf(os.Stderr, "Node Type = %v, Data = %v, Attr = %v\n\n", n.Type, n.Data, n.Attr)

    if n.Type == html.ElementNode && n.Data == "a" {
        for _, a := range n.Attr {
            if a.Key == "href" {
                links = append(links, a.Val)
            }
        }
    }

    for c := n.FirstChild; c != nil; c = c.NextSibling {
        links = visit(links, c)
    }

    return links
}

// Return array of links found in html doc downloaded from url
func getLinks(url string) []string {
    fmt.Println("Fetching links in", url)

    // http get
    resp, err := http.Get(url)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%v\n", err)
        return nil
    }
    defer resp.Body.Close()


    doc, err := html.Parse(resp.Body)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%v\n", err)
        os.Exit(1)
    }
   
    return visit(nil, doc)    
}

// Downloader contains informations of a downloading task
type Downloader struct {
    URL string
    FileName string
    ContentLength uint64
	BytesWritten uint64
    PercentDone uint64
    WaitGroup *sync.WaitGroup
    Error error
    Done bool
}

// writeCount implements the io.Writer interface which only keeps the total bytes
// without writing anything.
// We can pass this into io.TeeReader() which so we can track total bytes been read
func (dl *Downloader) Write(p []byte) (int, error) {
    n := len(p)
	dl.BytesWritten += uint64(n)
    dl.PercentDone = uint64(float64(dl.BytesWritten) / float64(dl.ContentLength) * 100)
	return n, nil
}

func (dl Downloader) getProgress() string {
	// Clear the line by using a character return to go back to the start and remove
	// the remaining characters by filling it with spaces
	//fmt.Printf("\r%s", strings.Repeat(" ", 80))

	// Return again and print current status of download
	// We use the humanize package to print the bytes in a meaningful way (e.g. 10 MB)
    if (dl.Error == nil) {
        return fmt.Sprintf("%s %s/%s (%d%%)", dl.FileName, humanize.Bytes(dl.BytesWritten), humanize.Bytes(dl.ContentLength), dl.PercentDone)
    } else {
        return fmt.Sprintf("%v", dl.Error)
    }
}

func getFileName(URL string) string {
    tokens := strings.Split(URL, "/")
	fileName := tokens[len(tokens) - 1]

    // Unescape "%20" to space
    fileName, _ = url.PathUnescape(fileName)

    return fileName
}

// Download the mp3 file in the url
func downloadUrl(dl *Downloader) {
    defer dl.WaitGroup.Done()

	// Create the file, but give it a tmp file extension, this means we won't overwrite a
	// file until it's downloaded, but we'll remove the tmp extension once downloaded.
	outFile, err := os.Create(dl.FileName + ".tmp")
	if err != nil {
        dl.Error = err
		return
	}
	defer outFile.Close()

    // Get the url
    resp, err := http.Get(dl.URL)
    if err != nil {
        dl.Error = err
        return
    }
    defer resp.Body.Close()

    // Get the ContentLength in Header
    if resp.ContentLength > 0 {
        dl.ContentLength = uint64(resp.ContentLength)
    } else {
        dl.ContentLength = 0
    }

	// Create our progress reporter and pass it to be used alongside our writer
	_, err = io.Copy(outFile, io.TeeReader(resp.Body, dl))
	if err != nil {
        dl.Error = err
		return
	}

	err = os.Rename(dl.FileName + ".tmp", dl.FileName)
	if err != nil {
        dl.Error = err
		return
	}

    dl.Done = true
}

// Downloaders contains a map of all Downloaders
type Downloaders struct {
    downloaders map[int]*Downloader
}

// Collect and print downloaders progress
// This function blocks until every downloader is done or fail
func (downloaders Downloaders) printOverallProgress() {
    // hide terminal cursor
    //fmt.Printf("\033[8m")

    // infinite for loop...
    for {
        numberDone := 0

        sortedKeys := make([]int, 0, len(downloaders.downloaders))
        for key := range downloaders.downloaders {
	        sortedKeys = append(sortedKeys, key)
        }
        sort.Ints(sortedKeys) //sort by key
        for _, key := range sortedKeys {
            fmt.Print(downloaders.downloaders[key].getProgress())
            fmt.Printf("\033[K") // Erases from the current cursor position to the end of the current line.
            fmt.Printf("\n")

            if downloaders.downloaders[key].Done || downloaders.downloaders[key].Error != nil {
                numberDone++
            }
        }
        if (numberDone == len(downloaders.downloaders)) {
            break
        }

        // Moves the cursor up by ${count_of_downloaders} rows.
        // The \033[ is the so-called escape sequence, check
        // http://www.termsys.demon.co.uk/vtansi.htm
        fmt.Printf("\033[%dA", len(downloaders.downloaders))

        time.Sleep(1000 * time.Millisecond)
    }

    // Moves the cursor down by ${count_of_downloaders} rows.
    fmt.Printf("\033[%dB", len(downloaders.downloaders))

    // reset terminal cursor
    //fmt.Printf("\033[0m")
}

func main() {
    var threads int
    var wgDownloadUrl sync.WaitGroup
    var url string // The url to scan links for files
    var downloaders Downloaders

    // Bind the flag to a variable.
    flag.IntVar(&threads, "t", 1, "Number of threads to download")

    // Override flag.Usage()
    flag.Usage = func() {
        fmt.Fprintf(os.Stderr, "Usage: wgot [option] URL\n")
        fmt.Fprintf(os.Stderr, "Options:\n")
        flag.PrintDefaults()
    }

    // Parse parses flag definitions from the argument list.
    flag.Parse()

    // Treat arguments remaining after flags have been processed as url
    if flag.NArg() > 0 {
        url = flag.Arg(0)
        fmt.Println("Downloading files in ", url)
    } else {
        flag.Usage()
        os.Exit(1)
    }

    links := getLinks(url)
//    downloaders.init()
    downloaders.downloaders = make(map[int]*Downloader)
    for i, link := range links {
        fileName := getFileName(link)

        // Skipping non-MP3 file
        if (!strings.HasSuffix(fileName, ".mp3")) {
            continue
        }        

        downloader := Downloader{}
        downloader.URL = link
        downloader.FileName = fileName
        downloader.WaitGroup = &wgDownloadUrl

        // Add this go routine to wait group
        wgDownloadUrl.Add(1)

        // Launch go routine for downloading link
        go downloadUrl(&downloader)

        downloaders.downloaders[i] = &downloader
    }

    // Collect and print download status
    go downloaders.printOverallProgress()

    // Wait for go routines to be finished
    wgDownloadUrl.Wait()
    fmt.Println("Done")
}
